<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "util/database.util.php";
require_once "exceptions/unsupported_operation.exception.php";
require_once "exceptions/connection.exception.php";

class TransactionManager {

  private ?mysqli $mysqli;
  private Logger $logger;
  private bool $transaction_started = false;

  private static ?TransactionManager $instance = null;

  private function __construct() {
    $this->logger = new Logger("TransactionManager");
    $this->mysqli = new mysqli(DATABASE_URL.":".DATABASE_PORT, DATABASE_USER, DATABASE_PASSWORD);
    if($this->mysqli->connect_errno) {
      $this->mysqli = null;
      throw new ConnectionException("Connection to database failed.");
    }
    if(!$this->mysqli->select_db(DATABASE_NAME)) {
      $this->logger->warn("Database ".DATABASE_NAME." not available. Creating the database.");
      $create_database = "CREATE DATABASE IF NOT EXISTS ".DATABASE_NAME." COLLATE utf8_bin";
      $this->logger->debug("Executing query: $create_database");
      $result = $this->mysqli->query($create_database);
      if(false == $result) {
        throw new NotStoredException("Database neither installable nor reachable.");
      }
    }
  }

  private function __clone() {}

  private function __wakeup() {}

  public static function get_instance() {
    if(!static::has_instance()) {
      static::$instance = new TransactionManager();
    }
    return static::$instance;
  }

  public static function has_instance() {
    return static::$instance != null;
  }

  public function open_transaction(bool $autocommit) {
    $this->logger->debug("Starting transaction.");
    if(!$this->mysqli->begin_transaction(MYSQLI_TRANS_START_READ_WRITE)) {
      $msg = "Transaction not started.";
      $this->logger->error($msg);
      throw new ConnectionException($msg);
    }
    if(!$this->mysqli->autocommit($autocommit)) {
      $msg = "Autocommit option could not be set.";
      $this->logger->error($msg);
      throw new ConnectionException($msg);
    }
    $this->transaction_started = true;
  }

  public function commit() {
    if(!$this->transaction_started)
      throw new UnsupportedOperationException("Commit cannot be executed before opening a transaction.");
    $this->logger->debug("Committing transaction.");
    if(!$this->mysqli->commit()) {
      $msg = "Commit order not executed.";
      $this->logger->error($msg);
      throw new ConnectionException($msg);
    }
  }

  public function rollback() {
    if(!$this->transaction_started)
      throw new UnsupportedOperationException("Rollback cannot be executed before opening a transaction.");
    $this->logger->info("Rolling back current transaction.");
    $result = $this->mysqli->query("ROLLBACK");
    if(false == $result) {
      $msg = "Rollback order not executed.";
      $this->logger->error($msg);
      throw new ConnectionException($msg);
    }
  }

  public function close_connection() {
    if($this->mysqli != null)
      $this->mysqli->close();
  }

  public function get_connection() {
    return $this->mysqli;
  }

}


 ?>
