<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once "team/team.service.php";
include_once "logs/logger.php";
include_once "util/token.util.php";
include_once "exceptions/missing_parameters.exception.php";
include_once "exceptions/parameter_type.exception.php";

class TeamController {

  public function __construct() {
    $this->service = new TeamService();
    $this->logger = new Logger("TeamController");
    $this->token_util = new TokenUtil();
  }

  public function add_team() {
    $this->logger->debug("Add team called.");

    $user_id = $this->token_util->check_token();

    if(!isset($_POST["name"])) throw new MissingParametersException("Name of team needed.");
    if(!isset($_POST["principal"])) throw new MissingParametersException("Principal needed.");

    $name = $_POST["name"];
    $principal = intval($_POST["principal"]);

    if(gettype($name) !== "string") throw new ParameterTypeException("Name isn't a string.");
    if(gettype($principal) !== "integer") throw new ParameterTypeException("Principal not given as number.");

    $created = $this->service->add_team($user_id, $principal, $name);

    $json_output = array(
      "id" => $created->get_id(),
      "name" => $created->get_name(),
      "principal" => $created->get_principal()
    );

    echo json_encode($json_output);
  }

  public function fetch_teams() {
    $this->logger->debug("fetch_teams() called");

    $user_id = $this->token_util->check_token();

    $fetched = $this->service->fetch_teams($user_id);

    $json_output = array();

    foreach ($fetched as $team) {
      $team_json = array(
        "id" => $team->get_id(),
        "principal" => $team->get_principal(),
        "name" => $team->get_name(),
        "members" => array()
      );
      foreach ($team->get_members() as $member) {
        $member_json = array(
          "id" => $member->get_id(),
          "username" => $member->get_username(),
          "forename" => $member->get_forename(),
          "name" => $member->get_name()
        );
        $team_json["members"][] = $member_json;
      }
      $json_output[] = $team_json;
    }

    echo json_encode($json_output);
  }

  public function assign_team() {
    $this->logger->debug("assign_team() called");

    $user_id = $this->token_util->check_token();

    if(!isset($_POST["user"])) throw new MissingParametersException("User to assign missing.");
    if(!isset($_POST["team"])) throw new MissingParametersException("Team to assign to missing.");

    $asignee = intval($_POST["user"]);
    $team = intval($_POST["team"]);

    if(gettype($asignee) !== "integer") throw new ParameterTypeException("Asignee not given as number.");
    if(gettype($team) !== "integer") throw new ParameterTypeException("Team not given as number.");

    $this->service->assign_team($user_id, $asignee, $team);
  }

}

 ?>
