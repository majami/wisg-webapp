<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'principal/principal.dal.php';
include_once 'user/user.service.php';
include_once 'exceptions/forbidden.exception.php';
include_once 'logs/logger.php';

class PrincipalService {

  private PrincipalDal $dal;
  private UserService $user_service;
  private Logger $logger;

  public function __construct() {
    $this->dal = new PrincipalDal();
    $this->user_service = new UserService();
    $this->logger = new Logger("PrincipleService");
  }

  public function create_principal(int $user_id, string $name) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN") {
      $this->logger->warn("User ".$user_id." is not allowed to create principals.");
      throw new ForbiddenException("Admin rights necessary.");
    }
    return $this->dal->store_principal($name);
  }

  public function fetch_principals($user_id) {
    $user = $this->user_service->fetch_user($user_id);
    if($user->get_role() != "ADMIN") {
      $this->logger->warn("User ".$user_id." is not allowed to view principals.");
      throw new ForbiddenException("Admin rights necessary.");
    }
    return $this->dal->fetch_principals();
  }

}

 ?>
