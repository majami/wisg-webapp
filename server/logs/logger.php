<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Logger {

  private $debug = "[DEBUG]";
  private $info =  "[INFO ]";
  private $warn =  "[WARN ]";
  private $error = "[ERROR]";

  private $time_format = "d.m.Y H:i,s";
  private $max_class_length = 20;

  private $class;

  public function __construct($class) {
    $this->class = $class;
  }

  public function debug($message) {
    $this->log($this->debug, $message);
  }

  public function info($message) {
    $this->log($this->info, $message);
  }

  public function warn($message) {
    $this->log($this->warn, $message);
  }

  public function error($message) {
    $this->log($this->error, $message);
  }

  private function log($level, $message) {
    if(!file_exists('logs/logs.log')) {
      fopen('logs/logs.log', 'w');
    }
    $now = new DateTime();
    if(filesize('logs/logs.log') > 200000) {
      $new_filename = 'logs-'.$now->format("d-m-Y--H-i-s").'.log';
      $abspath = dirname(__FILE__).'/';
      $moved = rename($abspath.'logs.log', $abspath.$new_filename);
      fopen($abspath.'logs.log', 'w');
      if(!$moved) {
        error_log("---- Log archive not created! -------\n", 3, 'logs/logs.log');
        error_log("Desired filename: ".$new_filename."\n", 3, 'logs/logs.log');
        error_log("-------------------------------------\n", 3, 'logs/logs.log');
      }
    }
    $class_string = "";
    if(strlen($this->class) <= $this->max_class_length) {
      $class_string = $this->class;
      for($i = strlen($this->class); $i <= $this->max_class_length; $i++) {
        $class_string = $class_string." ";
      }
    } else {
      $class_string = substr($this->class, 0, $this->max_class_length - 3)."...";
    }
    $output = $now->format($this->time_format)." ".$level." ".$class_string." ".$message."\n";
    error_log($output, 3, 'logs/logs.log');
  }
}

 ?>
