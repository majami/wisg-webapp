<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/***********************************************************
* Please only change the configuration marked by comments. *
***********************************************************/

if(!isset($_SESSION["test_env"]) || $_SESSION["test_env"] == false) {
  if(file_exists("dev.config.inc.php")) {
    require_once "dev.config.inc.php";
  } else {

    /*************************************
    * Change the configuration from here *
    *************************************/

    define("DATABASE_USER", "");
    define("DATABASE_PASSWORD", "");
    define("DATABASE_URL", "");
    define("DATABASE_PORT", 3306);
    define("DATABASE_NAME", "");

    define("TABLE_PREFIX", "game_");

    define("ALLOW_INSTALL", false);
    define("ALLOW_BACKUP", false);

    /*************************************
    * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ *
    *************************************/
  }
}


?>
