<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Task {

  private int $id;
  private string $name;
  private ?string $description;
  private int $xp;
  private int $principal;
  private bool $archived;

  public function __construct(int $id, string $name, ?string $description, int $xp, int $principal,
        bool $archived) {
    $this->id = $id;
    $this->name = $name;
    $this->description = $description;
    $this->xp = $xp;
    $this->principal = $principal;
    $this->archived = $archived;
  }

  public function get_id() {
    return $this->id;
  }

  public function get_name() {
    return $this->name;
  }

  public function set_name(string $name) {
    $this->name = $name;
  }

  public function get_description() {
    return $this->description;
  }

  public function set_description(?string $description) {
    $this->description = $descripton;
  }

  public function get_xp() {
    return $this->xp;
  }

  public function set_xp(int $xp) {
    $this->xp = $xp;
  }

  public function get_principal() {
    return $this->principal;
  }

  public function is_archived() {
    return $this->archived;
  }

  public function set_archived(bool $archived) {
    $this->archived = $archived;
  }

}

 ?>
