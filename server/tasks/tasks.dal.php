<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "logs/logger.php";
require_once "util/database.util.php";
require_once "exceptions/not_stored.exception.php";
require_once "tasks/task.dto.php";

class TasksDal {

  public function __construct() {
    $this->logger = new Logger("TaskDal");
  }

  public function store_task(int $principal, string $name, ?string $description, int $xp) {
    $mysqli = create_db_connection();
    $id = fetch_id($mysqli, "task", $this->logger);
    $query = "INSERT INTO ".TABLE_PREFIX."task ".
              "(id,name,description,xp,principal,archived) ".
              "VALUES (".
              $id.",".
              "'".$mysqli->real_escape_string($name)."',".
              "'".$mysqli->real_escape_string($description)."',".
              $xp.",".
              $principal.",".
              "false".
              ")";
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false == $result) {
      throw new NotStoredException("Task could not be stored.");
    }
    return new Task($id, $name, $description, $xp, $principal, false);
  }

  public function archive_task(int $task, bool $archived) {
    $mysqli = create_db_connection();
    $query = "UPDATE ".TABLE_PREFIX."task SET archived = $archived WHERE id = $task";
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false == $result) {
      throw new NotStoredException("Task could not be marked as archived.");
    }
  }

  public function add_points(int $task, int $task_asignee) {
    $mysqli = create_db_connection();
    $id = fetch_id($mysqli, "done_tasks", $this->logger);
    $done_date = date("Y-m-d H:i:s");
    $query = "INSERT INTO ".TABLE_PREFIX."done_tasks ".
              "(id,task,user,done_date) VALUES (".
              $id.",".
              $task.",".
              $task_asignee.",".
              "'".$mysqli->real_escape_string($done_date)."'".
              ")";
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false == $result) {
      throw new NotStoredException("Points could not be assigned");
    }
    return $id;
  }

  public function fetch_tasks(int $principal) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."task ".
              "WHERE principal = $principal ".
              "AND archived = false";
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false == $result) {
      throw new NotFoundException("Tasks could not be fetched");
    }
    $tasks = array();
    while($row = $result->fetch_assoc()) {
      $tasks[] = new Task(intval($row["id"]), $row["name"], $row["description"],
                  intval($row["xp"]), intval($row["principal"]), boolval($row["archived"]));
    }
    return $tasks;
  }

  public function update_task(int $id, string $name, ?string $description, int $xp) {
    $mysqli = create_db_connection();
    $query = "UPDATE ".TABLE_PREFIX."task SET ".
              "name = '".$mysqli->real_escape_string($name)."',".
              "description = '".$mysqli->real_escape_string($description)."',".
              "xp = $xp ".
              "WHERE id = $id";
    $this->logger->debug("Executing query: ".$query);
    $result = $mysqli->query($query);
    if(false == $result) {
      throw new NotStoredException("Task could not be updated.");
    }
  }

}

 ?>
