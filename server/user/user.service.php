<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'user/users.dal.php';
include_once 'token/token.service.php';
include_once 'logs/logger.php';
include_once 'user/user.dto.php';
include_once 'token/token.dto.php';
include_once 'exceptions/not_authorized.exception.php';
include_once 'exceptions/not_found.exception.php';
include_once 'exceptions/forbidden.exception.php';
include_once 'exceptions/missing_parameters.exception.php';
include_once 'util/transactionmanager.util.php';

class UserService {

  private UsersDal $dal;
  private TokenService $token_service;

  public function __construct() {
    $this->dal = new UsersDal();
    $this->token_service = new TokenService();
    $this->logger = new Logger("UserService");
  }

  public function user_login(string $username, string $password) {
    $not_authorized_reason = "User login not authorized.";
    try {
      $fetched = $this->dal->fetch_user($username);
      if(!$fetched->same_password($password)) {
        $this->logger->debug("Password check for user ".$username." failed.");
        throw new NotAuthorizedException($not_authorized_reason);
      }
      $this->logger->debug($username." authenticated, creating token");
      $token = $this->token_service->create_token($fetched->get_id());
      return $token;
    } catch(NotFoundException $e) {
      $this->logger->debug("User ".$username." not found in database.");
      throw new NotAuthorizedException($not_authorized_reason);
    }
  }

  public function authenticate(string $token) {
    try {
      $fetched = $this->token_service->fetch_token($token);
    } catch(NotFoundException $e) {
      throw new NotAuthorizedException();
    }
    return $fetched;
  }

  public function change_password(int $user_id, string $new_password) {
    $user = $this->dal->fetch_user_by_id($user_id);
    $user->set_password($new_password);
    $this->dal->update_user($user);
  }

  public function fetch_user(int $user_id, ?int $requester_id = null) {
    $user = $this->dal->fetch_user_by_id($user_id);
    if($requester_id == null)
      return $user;
    $requester = $this->dal->fetch_user_by_id($requester_id);
    if($user->get_role() != "ADMIN" && $requester->get_principal() != $user->get_principal())
      throw new ForbiddenException("Only allowed to fetch users of the same principal.");
    return $user;
  }

  public function create_user(int $creator, User $user) {
    $creating_user = $this->dal->fetch_user_by_id($creator);
    $creator_role = $creating_user->get_role();

    if($creator_role != "ADMIN") {
      $msg = "Not allowed to create users.";
      $this->logger->error($msg);
      throw new ForbiddenException($msg);
    }

    if(!($user->get_role() == "ADMIN" || $user->get_role() == "MASTER"
          || $user->get_role() == "PLAYER")) {
      $msg = "User not stored because role not supported.";
      $this->logger->error($msg);
      throw new ForbiddenException($msg);
    }

    if($user->get_role() == "MASTER" || $user->get_role() == "PLAYER") {
      if(null === $user->get_principal()) {
        $msg = "User not stored because principal missing.";
        $this->logger->error($msg);
        throw new MissingParametersException($msg);
      }
    }

    $ret_val = $this->dal->create_user($user);
    return $ret_val;
  }

  public function fetch_users(int $requester_id, ?int $given_principal) {
    $requester = $this->dal->fetch_user_by_id($requester_id);

    if("ADMIN" !== $requester->get_role() && "MASTER" !== $requester->get_role())
      throw new ForbiddenException("Players are not allowed to view other users.");

    if("ADMIN" === $requester->get_role() && null == $given_principal)
      throw new MissingParametersException("No principal given.");

    $principal = null;
    if("MASTER" == $requester->get_role()) {
      $principal = $requester->get_principal();
    } else {
      $principal = $given_principal;
    }

    return $this->dal->fetch_users($principal);
  }

  public function change_user_data(int $user_id, ?string $username, ?string $forename, ?string $name) {
    $user = $this->fetch_user($user_id);

    $tm = TransactionManager::get_instance();
    $tm->open_transaction(false);

    try {
      if(null != $username)
        $this->dal->change_username($user_id, $username);
      if(null != $forename)
        $this->dal->change_forename($user_id, $forename);
      if(null != $name)
        $this->dal->change_name($user_id, $name);
    } catch(Exception $e) {
      $tm->rollback();
      throw $e;
    }

    $tm->commit();
  }

  public function change_profile_picture(int $user_id, string $picture) {
    $this->dal->change_profile_picture($user_id, $picture);
  }

  public function fetch_profile_picture(int $user_id, int $owner_id) {
    $user = $this->fetch_user($user_id);
    $owner = $this->fetch_user($owner_id);
    if($user->get_principal() != $owner->get_principal() && $user->get_role() != "ADMIN")
      throw new ForbiddenException("Only allowed to fetch profile pictures from the same principal.");

    return $this->dal->fetch_profile_picture($owner_id);
  }

}

?>
