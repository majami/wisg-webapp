<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "config.inc.php";

$mysqli = $mysqli = new mysqli(DATABASE_URL, DATABASE_USER,
            DATABASE_PASSWORD, DATABASE_NAME, DATABASE_PORT);
$data = json_decode($_POST["data"], true);

foreach($data as $table => $content) {
  $delete = "DELETE FROM ".$table;
  echo "Executing query: ".$delete;
  $mysqli->query($delete);
  foreach($content as $row) {
    $query = "INSERT INTO ".$table." (";
    foreach($row as $column => $value) {
      $query = $query.$column.",";
    }
    $query = rtrim($query, ",");
    $query = $query.") VALUES (";
    foreach($row as $column => $value) {
      if(gettype($value) == "string")
        $query = $query."'".$value."',";
      elseif(null == $value)
        $query = $query."null,";
      else
        $query = $query.$value.",";
    }
    $query = rtrim($query, ",");
    $query = $query.")";
    echo "Executing query: ".$query."<br>";
    $result = $mysqli->query($query);
    if(false === $result) {
      echo "Not able to insert line.<br>";
    }
  }
}

 ?>
