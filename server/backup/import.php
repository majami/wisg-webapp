<!--
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->

<html>
  <head>
    <title>Daten importieren</title>
  </head>

  <body>
    <h1>Daten importieren</h1>
    <p> <strong>Warnung!</strong>  Alle Tabellen müssen
    vor dem Import leer sein. Außerdem müssen alle
    Tabellen bereits existieren.</p>

    <form action="/import/run" method="POST">
      <label for="data">Zu importierende Daten</label><br>
      <textarea name="data"></textarea><br>
      <button type="submit">Importieren</button>
    </form>
  </body>

</html>
