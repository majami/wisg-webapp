<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Bonus {

  private int $id;
  private int $principal;
  private string $name;
  private string $type;
  private float $value;

  public function __construct(int $id, string $name, string $type, float $value, int $principal) {
    $this->id = $id;
    $this->name = $name;
    $this->type = $type;
    $this->value = $value;
    $this->principal = $principal;
  }

  public function get_id() {
    return $this->id;
  }

  public function get_name() {
    return $this->name;
  }

  public function get_type() {
    return $this->type;
  }

  public function get_value() {
    return $this->value;
  }

  public function get_principal() {
    return $this->principal;
  }

}

 ?>
