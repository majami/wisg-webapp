<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once "logs/logger.php";
include_once "util/database.util.php";

class RankingDal {

  private Logger $logger;

  public function __construct() {
    $this->logger = new Logger("RankingDal");
  }

  public function fetch_points_sum_completed_tasks(int $user_id) {
    $mysqli = create_db_connection();
    $query = "SELECT SUM(".TABLE_PREFIX."task.xp) AS points FROM ".TABLE_PREFIX."task ".
              "JOIN ".TABLE_PREFIX."done_task ON ".TABLE_PREFIX."task.id = ".TABLE_PREFIX."done_task.task ".
              "WHERE ".TABLE_PREFIX."done_task.user = $user_id ".
              "GROUP BY ".TABLE_PREFIX."task.user";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotFoundException("Points could not be calculated.");
    $row = $result->fetch_assoc();
    return floatval($row["points"]);
  }

  public function fetch_completed_tasks(int $user_id) {
    $mysqli = create_db_connection();
    $query = "SELECT d.task AS task, t.xp AS xp, d.id AS id ".
              "FROM ".TABLE_PREFIX."done_tasks d ".
              "JOIN ".TABLE_PREFIX."task t ON d.task = t.id ".
              "WHERE d.user = $user_id";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotFoundException("Done tasks could not be fetched.");
    $tasks = array();
    while($row = $result->fetch_assoc()) {
      $tasks[] = array(
        "task" => intval($row["task"]),
        "xp" => floatval($row["xp"]),
        "id" => intval($row["id"])
      );
    }
    return $tasks;
  }

  public function fetch_relative_boni(int $id) {
    return $this->fetch_boni($id, "RELATIVE");
  }

  public function fetch_absolute_boni(int $id) {
    return $this->fetch_boni($id, "ABSOLUTE");
  }

  private function fetch_boni(int $id, string $type) {
    $mysqli = create_db_connection();
    $query = "SELECT * FROM ".TABLE_PREFIX."bonus b ".
              "JOIN ".TABLE_PREFIX."gained_bonus g ON b.id = g.bonus ".
              "WHERE type like '$type' AND task = $id ".
              "ORDER BY value ASC";
    $this->logger->debug("Executing query: $query");
    $result = $mysqli->query($query);
    if(false == $result)
      throw new NotFoundException("$type boni for task not fetched.");
    $gained_boni = array();
    while($row = $result->fetch_assoc()) {
      $gained_boni[] = floatval($row["value"]);
    }
    return $gained_boni;
  }

}

 ?>
