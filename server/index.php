<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

require_once "logs/logger.php";
require_once "exceptions/forbidden.exception.php";
require_once "config.inc.php";
require_once "util/transactionmanager.util.php";

$logger = new Logger("INDEX");

header('Access-Control-Allow-Origin: *');
if($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
  $logger->info("CORS resource request");
  header('Access-Control-Allow-Headers: *');
  header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
  header('Access-Control-Max-Age: 86400');
  die();
}

$path = explode("?", $_SERVER["REQUEST_URI"])[0];

$last_char = substr($path, strlen($path) - 1, 1);
if($last_char == "/") {
  $path = substr($path, 0, strlen($path) - 1);
}

try {

  switch ($path) {

    case "/test":
    echo "SUCCESS";
    break;

    case "/install":
    if(ALLOW_INSTALL)
      require_once "install/install.php";
    else
      throw new ForbiddenException("Installation not allowed on this server.");
    break;

    case "/export":
    if(ALLOW_BACKUP)
      require_once "backup/export.php";
    else
      throw new ForbiddenException("Export not allowed on this server.");
    break;

    case "/export/run":
    if(ALLOW_BACKUP)
      require_once "backup/exporter.php";
    else
      throw new ForbiddenException("Export not allowed on this server.");
    break;

    case "/import":
    if(ALLOW_BACKUP)
      require_once "backup/import.php";
    else
      throw new ForbiddenException("Import not allowed on this server.");
    break;

    case "/import/run":
    if(ALLOW_BACKUP)
      require_once "backup/importer.php";
    else
      throw new ForbiddenException("Import not allowed on this server.");
    break;

    case '/api/0.1/users/login':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->login();
    break;

    case '/api/0.1/users/change-password':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->change_password();
    break;

    case '/api/0.1/users/change-user-data':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->change_user_data();
    break;

    case '/api/0.1/users/create':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->create_user();
    break;

    case '/api/0.1/users/whoami':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->whoami();
    break;

    case '/api/0.1/users/fetch':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->fetch_users();
    break;

    case '/api/0.1/user':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->fetch_user();
    break;

    case '/api/0.1/user/profile-picture/fetch':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->fetch_profile_picture();
    break;

    case '/api/0.1/user/profile-picture/change':
    require_once 'user/user.controller.php';
    $controller = new UserController();
    $controller->change_profile_picture();
    break;

    case '/api/0.1/principal/create':
    require_once 'principal/principal.controller.php';
    $controller = new PrincipalController();
    $controller->create_principal();
    break;

    case '/api/0.1/principal/fetch':
    require_once 'principal/principal.controller.php';
    $controller = new PrincipalController();
    $controller->fetch_principals();
    break;

    case '/api/0.1/team/create':
    require_once 'team/team.controller.php';
    $controller = new TeamController();
    $controller->add_team();
    break;

    case '/api/0.1/teams/fetch':
    require_once 'team/team.controller.php';
    $controller = new TeamController();
    $controller->fetch_teams();
    break;

    case '/api/0.1/team/assign':
    require_once 'team/team.controller.php';
    $controller = new TeamController();
    $controller->assign_team();
    break;

    case '/api/0.1/tasks/create':
    require_once 'tasks/tasks.controller.php';
    $controller = new TasksController();
    $controller->create_task();
    break;

    case '/api/0.1/tasks/change':
    require_once 'tasks/tasks.controller.php';
    $controller = new TasksController();
    $controller->modify_task();
    break;

    case '/api/0.1/tasks/fetch':
    require_once 'tasks/tasks.controller.php';
    $controller = new TasksController();
    $controller->fetch_tasks();
    break;

    case '/api/0.1/tasks/done':
    require_once 'tasks/tasks.controller.php';
    $controller = new TasksController();
    $controller->mark_done();
    break;

    case '/api/0.1/bonus/create':
    require_once 'bonus/bonus.controller.php';
    $controller = new BonusController();
    $controller->create_bonus();
    break;

    case '/api/0.1/bonus/fetch':
    require_once 'bonus/bonus.controller.php';
    $controller = new BonusController();
    $controller->fetch_boni();
    break;

    case '/api/0.1/ranking':
    require_once 'ranking/ranking.controller.php';
    $controller = new RankingController();
    $controller->get_ranking();
    break;

    default:
    $logger->warn("Requested path $path does not exist.");
    require_once '../index.html';
    break;
  }

} catch (MissingParametersException | ParameterTypeException $pe) {
  header('HTTP/1.1 400 Bad Request');
  $GLOBALS["http_status"] = 400;
  $logger->error($pe->__toString());
  echo "Parameters missing or of wrong type, request cancelled";
} catch(ForbiddenException $fe) {
  header('HTTP/1.1 403 Forbidden');
  $GLOBALS["http_status"] = 403;
  $logger->error($fe->__toString());
  echo "FORBIDDEN";
} catch (Throwable $e) {
  header('HTTP/1.1 500 Internal Server Error');
  $GLOBALS["http_status"] = 500;
  $logger->error($e->__toString());
  echo $e->getMessage();
} finally {
  if(TransactionManager::has_instance())
    TransactionManager::get_instance()->close_connection();
}

?>
