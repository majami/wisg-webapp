# WISG #
Welcome to the 'Work is a game' application, short WISG. This guide will show you how to
use and install the application and how to participate in the development.

## About ##

WISG is an application for integrating gamification into the daily business life. Under
certain rules, employees can gain experiance points (XP). A ranking shows who's the
best of a team.

## Installation ##

### System requirements ##

- Apache Webserver (Others might work, too. You'll need to
provide an .htaccess replacement by your own.)
- PHP 7.4
- MySQL Database (MariaDB preferred)

Users have to access the application with a modern browser with JavaScript enabled.

### How to install ###

You can run the client and the server application on one single host, or you can split, e.g. for
building a Cordova App with the client code.

**Installing in one webserver:** Download the bundled application (*wisg-bundle-X.X.X.tar.gz*) and
extract the code to your webserver's root. Edit the *config.inc.php* file within the *server/* directory
with your configuration. Make shure to set *ALLOW_INSTALL* to *true*. After that, head over to
yourdomain.com/install (where [yourdomain] is your domain) and let the script run. If no error occurred,
you're all set up!

**Installing only the WISG server:** It's the same as with the bundled application. Just extract the
sources to your webserver and follow the steps mentioned above.

**Installing only the WISG client:** Just extract the sources. No further steps needed.

## Development ##
While the client is built on HTML, CSS and Javascript, the server is a plain PHP application.
Everyone is welcome to enhance and extend the application. However, please
read the guidelines carefuly.

### Development guidelines ###
As the rights to the main repository are restricted, make shure to fork
it and use the fork for your development. We use the rebase strategy
instead of merge, so please make shure to update your development branch
doing a rebase.

When creating a branch, please make shure to add one of `feature/`, `bugfix/` or
`hotfix/` before your branch name. Feel free to find a name by yourself. After
finishing your work, please sqash all commits together as there will be only
one integrated commit. When finished, please create a pull request to the main
repository. `feature` and `bugfix` branches will be integrated into the latest
`release` branch, while `hotfix` branches cause a minor release.

Please always make shure to keep your code clean an well readable. It is
recommended to follow the style of the existing code. If needed, improvements
are welcome but may be rejected if not.

### Tipps ###
Make shure that the `logs` folder is writable. The system
will place logs here. The folder should be writable for the user the local
webser is running on (in case of XAMPP it's *deamon*).

### Contributing ###
When contributing to the project, you agree to the CLA (Contributor licence aggreement).
This agreement ensures the fair use of this application's code for you and the maintainer.
Please refer to the `CLA.md` file for information.
