#! /bin/bash

#        WISG - make work a game
#        Copyright (C) 2020  Eric Fischer and all contributors
#
#        This program is free software: you can redistribute it and/or modify
#        it under the terms of the GNU General Public License as published by
#        the Free Software Foundation, either version 3 of the License, or
#        (at your option) any later version.
#
#        This program is distributed in the hope that it will be useful,
#        but WITHOUT ANY WARRANTY; without even the implied warranty of
#        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#        GNU General Public License for more details.
#
#        You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.

jsbuild='java -jar ./lib/closure.jar --js_output_file ./scripts.js --compilation_level SIMPLE '

jsbuild=$jsbuild'--js ./sjs/utils.sjs '
jsbuild=$jsbuild'--js ./sjs/localstoragemanager.sjs '
jsbuild=$jsbuild'--js ./sjs/logger.sjs '

for f in `find ./sjs -iname '*.service.sjs'`
do
  jsbuild=$jsbuild'--js '$f' '
done

for f in `find ./sjs -iname '*.controller.sjs'`
do
  jsbuild=$jsbuild'--js '$f' '
done

for f in `find ./sjs -iname '*.content.sjs'`
do
  jsbuild=$jsbuild'--js '$f' '
done

for f in `find ./sjs -iname '*.view.sjs'`
do
  jsbuild=$jsbuild'--js '$f' '
done

jsbuild=$jsbuild'--js ./sjs/init.sjs'

echo "Running Closure build command: "$jsbuild

eval $jsbuild

scss scss/styles.scss styles.css --style compressed
