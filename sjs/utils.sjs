/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Utils {

  constructor() {
    this.log = new Logger("Utils");
  }

  loadTemplate(container, name, callback) {
    this.log.debug("Loading template: " + name + " into " + container);
    $("head").append(
      "<link rel=\"prefetch\" type=\"text/html\" href=\"templates/" + name + "\">"
    );
    $(container).load("templates/" + name, "", callback);
  }

  loadMultipleTemplates(templateConfigs, callback) {
    let loaded = 0;
    let ref = this;
    templateConfigs.forEach(function(config) {
      ref.loadTemplate(config.container, config.name, function() {
        loaded++;
        if(loaded == templateConfigs.length) {
          callback();
        }
      })
    });
  }

  // found at https://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
  findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
  }

  createAjaxHeader() {
    let lsm = new LocalStorageManager();
    let header = {"token": lsm.getToken()};
    this.log.debug("Created header: " + JSON.stringify(header));
    return header;
  }

  /**
  Adds a spinner to the given button.
  If replaceIcon is true, the Fontawesome icon will be replaced with the spinner.
  **/
  addSpinner(button, replaceIcon) {
    if(true == replaceIcon) {
      button.find("span.fa").hide();
      button.find("span.fas").hide();
    }
    let content = button.html();
    button.html(
      "<span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span>"
      + content
    );
  }

  /**
  Removes a spinner from the given button.
  If replaceIcon is true, the Fontawesome icon will be recovered after the spinner is removed.
  **/
  removeSpinner(button, replaceIcon) {
    button.find("span.spinner-border").remove();
    button.find("span.fa").show();
    button.find("span.fas").show();
  }

  getImageDataURL(fileInput, callback) {
    this.log.debug("Reading data from file input.");
    let fr = new FileReader();
    let file = fileInput[0].files[0];
    fr.readAsDataURL(file);
    fr.onload = function(e) {
      callback(e.target.result);
    };
  }
}
