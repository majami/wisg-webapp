/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class LoginController {

  constructor() {
    this.lsm = new LocalStorageManager();
    this.utils = new Utils();
    this.userService = new UserService();
    this.log = new Logger("LoginController");
  }

  checkServer(successCallback, errorCallback) {
    this.log.debug("Checking server connection.");
    let ref = this;
    let apiHost = window.location.protocol + "//" + window.location.hostname;
    $.ajax({
      url: apiHost + "/test",
      method: "GET",
      success: function(data, status, xhr) {
        ref.log.debug("The server runs on the same host.");
        ref.lsm.setServerUrl(apiHost);
        successCallback();
      },
      error: function(xhr, status, error) {
        ref.log.debug("The server on this host isn't reachable. Status: " + status);
        errorCallback();
      }
    });
  }

  login(username, password, server, errorCallback) {
    this.log.debug("login() called");
    this.lsm.setServerUrl(server);

    let request = {
      "username": username,
      "password": password
    };

    let ref = this;
    $.ajax({
        url: this.lsm.getServerUrl() + "/api/0.1/users/login",
        method: "POST",
        datatype: "json",
        data: request,
        success: function(data, status, xhr) {
          ref.log.log("Login request successful. Data: " + data);
          var response = JSON.parse(data);
          window.localStorage.setItem("token", response.token);
          // redirect to overview
          new OverviewView();
        },
        error: function(xhr, status, error) {
          errorCallback();
        }
      });
  }

  checkRights() {
    this.log.debug("Checking rights of user");
    let ref = this;
    if(this.userService.checkSession()) {
      this.userService.whoami(function(user) {
        // the user is authenticated at the server, redirect to the overview
        new OverviewView();
      }, function() {
        ref.log.error("User rights not fetched.");
      });
    }
  }

}
