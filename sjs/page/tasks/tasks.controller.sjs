/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class TasksController {

  constructor() {
    this.log = new Logger("TasksController");
    this.utils = new Utils();
    this.lsm = new LocalStorageManager();
    this.userService = new UserService();
  }

  // TODO rights check needed!

  fetchTasks(successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let ref = this;
    $.ajax({
        url: ref.lsm.getServerUrl() + "/api/0.1/tasks/fetch",
        method: "GET",
        datatype: "json",
        data: {},
        headers: header,
        success: function(data, status, xhr) {
          var response = JSON.parse(data);
          ref.log.info("Tasks fetched.");
          successCallback(response);
        },
        error: function(xhr, status, error) {
          ref.log.warn("Tasks not fetched.");
          errorCallback();
        }
      });
  }

  fetchUsers(callback) {
    let ref = this;
    this.userService.whoami(function(user) {
      ref.userService.fetchUsers(user.principal, function(users) {
        callback(users);
      }, function() {
        ref.log.error("Users could not be fetched.");
      });
    }, function() {
      ref.log.error("Current user information could not be fetched.");
    })
  }

  fetchBoni(successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let ref = this;
    $.ajax({
        url: ref.lsm.getServerUrl() + "/api/0.1/bonus/fetch",
        method: "GET",
        datatype: "json",
        data: {},
        headers: header,
        success: function(data, status, xhr) {
          var response = JSON.parse(data);
          ref.log.info("Boni fetched.");
          successCallback(response);
        },
        error: function(xhr, status, error) {
          ref.log.warn("Boni not fetched.");
          errorCallback();
        }
      });
  }

  markAsDone(taskId, asignees, boni, archiveTask, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let archiveTaskText = archiveTask ? "true" : "false";
    let request = {
      task: taskId,
      asignees: asignees,
      boni: boni,
      archiveTask: archiveTaskText
    };
    this.log.debug("Sending request: " + JSON.stringify(request));
    let ref = this;
    $.ajax({
        url: ref.lsm.getServerUrl() + "/api/0.1/tasks/done",
        method: "POST",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.info("Marked task as done.");
          successCallback();
        },
        error: function(xhr, status, error) {
          ref.log.warn("Task not marked as done.");
          errorCallback();
        }
      });
  }

  editTask(id, name, description, xp, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let request = {
      id: id,
      name: name,
      description: description,
      xp: xp
    };

    let ref = this;
    $.ajax({
        url: ref.lsm.getServerUrl() + "/api/0.1/tasks/change",
        method: "POST",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.info("Task changed.");
          successCallback();
        },
        error: function(xhr, status, error) {
          ref.log.error("Task not changed.");
          errorCallback();
        }
      });
  }

}
