/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class OverviewView {

  constructor() {
    this.controller = new OverviewController();
    this.log = new Logger("OverviewView");
    this.utils = new Utils();

    this.log.debug("Creating overview.");
    if(this.controller.checkRights()) {
      $("#app *").remove();
      $("title").text("Übersicht");
      history.pushState({page:0}, $("title").text(), "?p=overview");
      let ref = this;
      this.utils.loadTemplate("#app", "overview/main.html", function() {
        ref._buildPage();
        ref._hookLogoutListener();
      });
    }
  }

  _buildPage() {
    this.log.debug("Building overview");
    let ref = this;
    this.controller.whoami(function(user) {
      ref.controller.fetchProfilePicture(user.id, function(image) {
        $("#profile-pic").attr("src", image);
      });
    });
    this.controller.getRole(function(role) {
      switch (role) {
        case "ADMIN":
          new AdminContent();
          break;
        case "MASTER":
          new GameMasterContent();
          break;
        case "PLAYER":
          new PlayerContent();
          break;
        default:
          ref.log.error("No content for role found.");
      }
    })
  }

  _hookLogoutListener() {
    this.log.debug("Setting up logout listener...");
    let ref = this;
    $("#logout").click(function(e) {
      e.preventDefault();
      ref.log.debug("Logging out...");
      ref.controller.logout();
    });
  }

}
