/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class TeamView {

  constructor() {
    this.controller = new TeamController();
    this.log = new Logger("TeamView");
    this.utils = new Utils();

    if(this.controller.checkRights()) {
      $("#app *").remove();
      $("title").text("Team verwalten");
      history.pushState({page:0}, $("title").text(), "?p=team");
      let ref = this;
      this.utils.loadTemplate("#app", "teams/main.html", function() {
        ref._buildPage();
      });
    }
  }

  _buildPage() {
    this.log.debug("Creating team page content.");
    this.teamsList = new TeamsList("#content");
    let ref = this;
    this.controller.fetchTeams(function(teams) {
      teams.forEach(function(team) {
        ref.teamsList.addEntry(team.id, team.name, team.principal, team.members);
      });
    }, function() {});
  }

}
