/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class UserService {

  constructor() {
    this.lsm = new LocalStorageManager();
    this.utils = new Utils();
    this.log = new Logger("UserService");
  }

  checkSession() {
    let token = this.lsm.getToken();
    this.log.debug("Token:" + token);
    if(token != null) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    this.lsm.deleteUser();
    this.lsm.deleteToken();
  }

  whoami(successCallback, errorCallback) {
    this.log.debug("Called whoami()");
    let header = this.utils.createAjaxHeader();
    let ref = this;
    $.ajax({
        url: this.lsm.getServerUrl() + "/api/0.1/users/whoami",
        method: "GET",
        headers: header,
        success: function(data, status, xhr) {
          ref.log.debug("Fetched logged in user: " + data);
          let user = JSON.parse(data);
          ref.lsm.setUser(user);
          successCallback(user);
        },
        error: function(xhr, status, error) {
          ref.log.error("Rights not fetched. Logging out. Status: " + status + " Error: " + error);
          ref.logout();
          errorCallback();
        }
      });
  }

  fetchUsers(principal, successCallback, errorCallback) {
    this.log.debug("Called fetchUsers()");
    let header = this.utils.createAjaxHeader();
    let request = {principal: principal};
    let ref = this;
    $.ajax({
        url: this.lsm.getServerUrl() + "/api/0.1/users/fetch",
        method: "GET",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.debug("Fetched users: " + data);
          let response = JSON.parse(data);
          successCallback(response);
        },
        error: function(xhr, status, error) {
          ref.log.error("Fetching users failed.");
          errorCallback();
        }
      });
  }

  fetchUser(id, successCallback, errorCallback) {
    this.log.debug("Called fetchUser()");
    let header = this.utils.createAjaxHeader();
    let request = {id: id};
    let ref = this;
    $.ajax({
        url: this.lsm.getServerUrl() + "/api/0.1/user",
        method: "GET",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.debug("Fetched user: " + data);
          let response = JSON.parse(data);
          successCallback(response);
        },
        error: function(xhr, status, error) {
          ref.log.error("Fetching user failed.");
          errorCallback();
        }
      });
  }

  fetchProfilePicture(ownerId, successCallback, errorCallback) {
    this.log.debug("Called fetchProfilePicture()");
    let header = this.utils.createAjaxHeader();
    let request = {owner: ownerId};
    let ref = this;
    $.ajax({
        url: this.lsm.getServerUrl() + "/api/0.1/user/profile-picture/fetch",
        method: "GET",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.debug("Fetched profile picture: " + data);
          successCallback(data);
        },
        error: function(xhr, status, error) {
          ref.log.warn("Fetching profile picture failed.");
          errorCallback();
        }
      });
  }

  changeProfilePicture(imageSrc, successCallback, errorCallback) {
    this.log.debug("Called changeProfilePicture()");
    let header = this.utils.createAjaxHeader();
    let request = {image: imageSrc};
    let ref = this;
    $.ajax({
        url: this.lsm.getServerUrl() + "/api/0.1/user/profile-picture/change",
        method: "POST",
        datatype: "json",
        data: request,
        headers: header,
        success: function(data, status, xhr) {
          ref.log.debug("Changed profile picture");
          successCallback();
        },
        error: function(xhr, status, error) {
          ref.log.error("Changing profile picture failed.");
          errorCallback();
        }
      });
  }

}
